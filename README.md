# Ansible Role: MOTD Notifier

An Ansible role that deploys a dynamic MOTD based on the original Debian MOTD Update Notifier.

https://oitibs.com/debian-stretch-dynamic-motd/


## Requirements
None.

## Role Variables
None.


### Deploy

```bash
$ ansible-playbook deploy.yml -K
```

### Example

```
 _                 _ _               _   
| | ___   ___ __ _| | |__   ___  ___| |_ 
| |/ _ \ / __/ _` | | '_ \ / _ \/ __| __|
| | (_) | (_| (_| | | | | | (_) \__ \ |_ 
|_|\___/ \___\__,_|_|_| |_|\___/|___/\__|
                                         

Welcome to Raspbian GNU/Linux 9.11 (stretch) (4.19.66-v7+).

System information as of: Sun Jun  7 01:41:46 BST 2020

System Load:	0.02	IP Address:	10.1.0.10
Memory Usage:	26.6%	System Uptime:	9 days
Usage On /:	69%	Swap Usage:	12.2%
Local Users:	0	Processes:	147

Last login: Sun Jun  7 01:04:06 2023 from 19.1.0.11


```
## Licence

MIT/BSD
